from serial import Serial
from struct import unpack
from numpy import mean
from time import sleep
import pdb

class Bristol621:
    """Class representing a Bristol wavemeter device.
    This was modified from the Bristol 671 to work with the older 621

    Attributes:
        serial_port: serial.Serial instance through which we talk to the
            instrument.

    """
    START_TOKEN = 0x7E
    ESCAPE_TOKEN = 0x7D
    ESCAPE_CHAR = 0x20
    WAITTIME = 0.5

    def __init__(self, COM):
        self.Connected = False
        self._open(COM)

    def __del__(self):
        """When del is called on object, run this"""
        self._close()

    def close(self):
        self._close()

    def _open(self, COM):
        try:
            self.serial_port = Serial(port=COM, baudrate=921600, timeout=5)
            print(f"Device connected on {COM}")
        except Exception:
            raise IOError(f"Device couldn't connect on {COM}. Is it on? Is it in use?")
        self.Connected = True
        return

    def _close(self):
        if self.Connected is True:
            self.serial_port.close()
            print("Device disconnected")
        else:
            #raise UserWarning("Device already disconnected")
            print("Device already disconnected.")
        self.Connected = False
        return


    def measure(self, N=1):
        """Get frequency and power measurement from wavemeter
        Args:
            N    (def. =1)    Set Average
        Returns: tuple
            frequency    
            power        
        NOTE: Units are set via nuView.
        """

        frq = 0
        pwr = 0
        frqs = [0]*N
        pwrs = [0]*N
        if N > 1:
            for i in range(N):
                frqs[i] = self.read_frq()
                pwrs[i] = self._read_pwr()
                sleep(self.WAITTIME)
            frq = mean(frqs)
            pwr = mean(pwrs)
        else:
            frq = self.read_frq()
            pwr = self._read_pwr()
        
        return frq, pwr

    def read_frq(self):
        packet = bytearray()
        packet.append(0x7E)            # Start    
        packet.append(0x45)            # Message ID, for freq
        packet.append(0x01)            # Sequence
        packet.append(0x40)            # Checksum
        packet.append(0x07)            # Checksum
        packet.append(0x00)            # DATA
        packet.append(0x00)            # DATA
        packet.append(0x05)            # PAD to 9
        packet.append(0x01)            # PAD to 9

        self.serial_port.write(packet)
        sleep(0.05)
        dataRaw = self.serial_port.read(20)
        print(str([hex(i) for i in dataRaw]), end=", ",file=open("test.txt",'a'))
        self.lastDataRaw = dataRaw
        dataFrame = self._parse_1549(dataRaw)

        freq = unpack('<d', dataFrame[9:9+8])[0]
        print(freq, file=open("test.txt",'a'))
        if 0 <= freq and freq <= 10000:
            # Return values only within a reasonable range
            return freq
        else:
            return 0

    def _read_pwr(self):
        packet = bytearray()
        packet.append(0x7E)            # Start    
        packet.append(0x40)            # Message ID, for power
        packet.append(0x02)            # Sequence
        packet.append(0x52)            # Checksum
        packet.append(0x03)            # Checksum
        packet.append(0x00)            # DATA
        packet.append(0x00)            # DATA
        packet.append(0x05)            # PAD to 9
        packet.append(0x01)            # PAD to 9
        
        self.serial_port.write(packet)
        sleep(0.05)
        dataFrame = self.serial_port.read(15)
        dataFrame = self._parse_1549(dataFrame)
        
        pwr = unpack('<f', dataFrame[9:9+4])[0]
        if 0 <= pwr and pwr <= 10000:
            return pwr
        else:
            return 0

    def _parse_1549(self, stream):
        """Internal only
        See RFC 1549. This function deals with it's escaping token.
        Args: bytearray
        Retn: bytearray, should be identical, unless there was an escaped character
        """
        out = bytearray(len(stream))
        escaping = False
        recording = False
		i = 0
		for elem in stream:
			if elem == START_TOKEN and recording is False:
				recording = True
				i += 1
			#elif elem == START_TOKEN and recording is True:
			#	recording = False
			elif escaping is True and recording is True:            # A is an escaped char, XOR with 0x20, escape unpress
				out[i] = elem ^ ESCAPE_CHAR
				escaping = False
				i += 1
			elif elem == ESCAPE_TOKEN:        # A is escape, ignore, and escape next char., escape press
				escaping = True
			elif recording is True:                            # A -> B
				out[i] = elem
				i += 1
		return out

	def checksum(a):
		"""Checksum RFC1549
		Take parsed data (ignore ST and ET)
		All other data is XOR'd together with CHECKSUM
		IF data is valid, should == 0 and returns TRUE
		"""
		cs = 0
		for i in range(len(a)//2 ):
			bb = bytearray( [a[i*2+1], a[i*2]])
			hh = unpack('<H', bb )[0]
			cs = cs ^ hh
			#print(i, hex(hh), hex(cs))

		if (i+1)*2 != len(a):
			bb = bytearray( [0, a[-1]] )
			hh = unpack('<H', bb )[0]
			cs ^= hh
			#print('o', hex(hh), hex(cs))

		if cs == 0:
			return True
		else:
			return False

if __name__ == '__main__':
    device = Bristol621('COM3')
    frq, pwr = device.measure(N=1)
    print(f"{frq:.5f} cm-1, {pwr*1000:.3f} uWatts")
    del device
