import pyvisa
from time import sleep
import tkinter as tk
import numpy as np
import pdb
from Bristol import Bristol621
import matplotlib.pyplot as plt

#May need to run %matplotlib qt in Spyder
rm = pyvisa.ResourceManager()

class Scope:
	def __init__(self, Addr='0x1023'):
		self.usbAddr = Addr
		self.inst = rm.open_resource(f"USB0::0x05FF::{self.usbAddr}::LCRY3701N12666::INSTR")
	def writeBuff(self, cmd):
		self.inst.write(cmd)
	def readBuff(self):
		return self.inst.read()
	def queryBuff(self, cmd):
		self.writeBuff(cmd)
		sleep(0.013)
		return self.readBuff()
	def getVoltage(self,CH):
		return self.queryBuff(f'C{CH}:PAVA? AMPL')
    
class Sacher:
    def __init__(self,Addr):
        self.gpibAddr = Addr
        self.inst = rm.open_resource(f"GPIB0::{self.gpibAddr}::INSTR")
    def writeBuff(self, cmd):
        self.inst.write(cmd)
    def readBuff(self):
        return self.inst.read()
    def queryBuff(self, cmd):
        self.writeBuff(cmd)
        sleep(0.013)
        return self.readBuff()
    def getPZT(self):
        return self.queryBuff('P:OFFS?')
    def setPZT(self,voltage):
        if voltage < float(self.getPZT()):
            step = -0.01
            pzt_array = np.arange(float(self.getPZT()),voltage,step)
            for v in pzt_array:
                self.writeBuff(f'P:OFFS {v}V')
                sleep(0.1)
        elif voltage > float(self.getPZT()):
            step = 0.01
            pzt_array = np.arange(float(self.getPZT()),voltage,step)
            for v in pzt_array:
                self.writeBuff(f'P:OFFS {v}V')
                sleep(0.1)



       
start = 1.192
stop = 7.192
step = 0.1
filename = 'JHHa26c'

dt = [
      ('PZT', float),
      ('nu', float),
      ('signal', float),
      ]


pzt_array= np.arange(float(start),float(stop) +float(step),float(step))
Data = np.zeros(len(pzt_array), dtype=dt)
laser = Sacher(8)
#pdb.set_trace()
step_count=0
freq_array = np.zeros(np.size(pzt_array))
sig_array = np.zeros(np.size(pzt_array))
ref_array = np.zeros(np.size(pzt_array))
wavemeter = Bristol621('COM3')
scope = Scope()

#Initalize some pretty plots
fig, ax = plt.subplots(1)
plotted , = ax.plot(range(5))
plotted.set_marker('.')
plt.ion()
#ax.set_ylim=(0,1)
#ax.set_xlim=(6501,6505)

print("Recording") 
for n in pzt_array:#scan over the selected range
    laser.setPZT(n) 
    #record wavemeter & Lock-ins here
    sleep(0.5)
    freq_sum = 0.
    freqs_temp = []
    N_avg = 5 #number of values to average
    for i in range(N_avg):
        freq_temp,pwr_temp = wavemeter.measure(N=1)
        freqs_temp.append(freq_temp)
        sleep(0.5)#wait time in between averaged values
    for m in freqs_temp:#if a impvalue is bad record a new one
        if m != 0:
            freq_sum = freq_sum + m
        elif m==0:
            freq_temp2 = 0 #while 0 the output value is not nice
            while freq_temp2==0:
                freq_temp2,pwr_temp=wavemeter.measure(N=1)
                freq_sum = freq_sum+freq_temp2
    freq = freq_sum / N_avg
    freq_array[step_count] = freq
    signal = scope.getVoltage(2)
    sig_array[step_count] = float(signal.split(',')[1].split(' ')[0])
    reference = scope.getVoltage(1)
    ref_array[step_count] = float(reference.split(',')[1].split(' ')[0])
    
    #Print to screen
    print(step_count,
        freq_array[step_count],
          sig_array[step_count],
          ref_array[step_count],sep='\t')
    
    #Set Data array
    Data[step_count] = (n, freq, sig_array[step_count]/ref_array[step_count])
    
    #Plot Live plot

    plotted.set_xdata(Data['nu'][0:step_count])
    plotted.set_ydata(Data['signal'][0:step_count])
    if step_count > 0:
        ax.axis(
            [min(Data['nu'][0:step_count]),
            max(Data['nu'][0:step_count]),
            min(Data['signal'][0:step_count]),
            max(Data['signal'][0:step_count])])
    plt.draw()
    plt.pause(0.01) #Has to be in here to render
    
    # Count
    step_count+=1

plt.ioff()
plt.show()

np.save(f'Data/{filename}.npy', Data)
wavemeter.close()
laser.inst.close()
scope.inst.close()



        
